## Static HTML Generator

Benutzt http://www.metalsmith.io/ zur Generierung von statischen HTML-Dateien auf Basis von Textinhalten (HTML/Markdown) und Handlebars-Templates


```bash 
yarn
bower install
node serve.js
```


## Test URLs

http://localhost:8080/beratung/de/

## Deployment
automatic deployment via bamboo except for produktkonfigurator-beratung.de (has to be done via ftp)



produktkonfigurator-beratung.de
https://www.checkdomain.de
Kundennummer: CO93463
Kundenzugang: produktkonfigurator-beratung@web.de
Passwort: XXX

FTP-Servername: host129.checkdomain.de 
FTP-Port: 21 (kein SSL!!!)
FTP Benutzer/Login: grhwsead
FTP Pass Standard(p) und am Ende: ! 


see:
https://drive.google.com/drive/folders/1ELwxIB626B3a4k2nTmfYVYCIOmBHzRxG


- build with node serve.js
- copy /dist/.htaccess to /htdocs
- copy /dist/index.php to /htdocs
- copy /dist/assets to /htdocs
- copy /dist/robots.txt to /htdocs
- copy /dist/scripts to /htdocs
- copy /dist/beratung/de/ to /htdocs