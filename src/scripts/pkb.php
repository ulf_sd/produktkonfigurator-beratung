<?php 
$emailTo = 'info@kaliber5.de';

error_reporting(E_ALL ^ E_NOTICE); // hide all basic notices from PHP

//If the form is submitted
if(isset($_POST['submitted'])) {

    $data = $_POST;

    unset ($data['submitted']);

    $errors = array();
	// require a name from user
	if(trim($data['pkName']) === '') {
        $errors[] =  'Bitte geben Sie Ihren Namen ein!';
	} else {
		$name = trim($data['pkName']);
	}
	
	// need valid email
	if(trim($data['pkEmail']) === '')  {
        $errors[] = 'Bitte geben Sie Ihre E-Mai Adresse ein';
	} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($data['pkEmail']))) {
        $errors[] = 'Ungültige E-Mail-Adresse!';
	} else {
		$email = trim($data['pkEmail']);
	}
		
	// we need at least some content
	if(trim($data['pkPhone']) === '') {
        $errors[] = 'Bitte geben Sie eine Telefonnumer an!';
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($data['comments']));
		} else {
			$comments = trim($data['comments']);
		}
	}

    $site = trim($_SERVER['HTTP_HOST']);
		
	// upon no failure errors let's email now!
	if(count($errors) == 0) {
		
		$subject = 'Nachricht von '.$site;
        $headers = 'From: ' . $name .' <'.$email.'>' . "\r\n" . 'Reply-To: ' . $email;

        $body = '';
        foreach ($data as $key => $data) {
            if(is_array($data)){
                $data = implode(", ", $data);
            }
            $body .=  $key . ': ' . $data. "\n\n";

        }
		$mailreply = mail($emailTo, $subject, $body, $headers);

        http_response_code(200);
        header('Content-type: application/json');
        echo json_encode(array(
            'sent' => $mailreply,
            'emailTo' => $emailTo,
            '$subject' => $subject,
            '$body' => $body,
            '$headers' => $headers,
            'date' => '19'
        ));

	}
    else {
        http_response_code(400);
        header('Content-type: application/json');
        echo json_encode(array(
            'errors' => $errors
        ));
    }
}
?>