// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $, window, document, undefined ) {

  "use strict";

  // undefined is used here as the undefined global variable in ECMAScript 3 is
  // mutable (ie. it can be changed by someone else). undefined isn't really being
  // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
  // can no longer be modified.

  // window and document are passed through as local variable rather than global
  // as this (slightly) quickens the resolution process and can be more efficiently
  // minified (especially when both are regularly referenced in your plugin).

  // Create the defaults once
  var pluginName = "handdrawn",
    defaults = {
      autoStart: true,
      speed: 1,
      transitionTiming: 'ease-in-out',
      reverseOrder: false
    };

  // The actual plugin constructor
  function Plugin ( element, options ) {
    this.element = element;
    this.$element = $(element);
    // jQuery has an extend method which merges the contents of two or
    // more objects, storing the result in the first object. The first object
    // is generally empty as we don't want to alter the default options for
    // future instances of the plugin
    this.settings = $.extend( {}, defaults, options, this.$element.data() );
    this._defaults = defaults;
    this._name = pluginName;
    this._lines = this.$element
      .find('path, line, polyline');
    this.init();
  }

  // Avoid Plugin.prototype conflicts
  $.extend(Plugin.prototype, {
    init: function () {
      // Place initialization logic here
      // You already have access to the DOM element and
      // the options via the instance, e.g. this.element
      // and this.settings
      // you can add more functions like the one below and
      // call them like so: this.yourOtherFunction(this.element, this.settings).
      this.initPaths();
      this.initTransitions();

      if (this.settings.autoStart) {
        this.start();
      }
    },
    initPaths: function () {
      var me = this;

      function lengthOfLine(x1, y1, x2, y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
      }

      // find lengths of lines
      this._lines
        .each(function(){
          var length = 0;
          switch (this.tagName) {
            case 'path':
              length = this.getTotalLength();
              break;
            case 'line':
              length = lengthOfLine(this.x1.baseVal.value, this.y1.baseVal.value, this.x2.baseVal.value, this.y2.baseVal.value);
              break;
            case 'polyline':
              var points = this.points;
              for (var i = 1; i<points.length; i++) {
                length += lengthOfLine(points[i-1].x, points[i-1].y, points[i].x, points[i].y);
              }
              break;
          }

          //this.setAttribute('handdrawn:length', length);
          this.handdrawnLength = length;

          if (length > 0) {
            this.style.strokeDasharray = length + ' ' + length;
            this.style.strokeDashoffset = length;
            // Trigger a layout so styles are calculated & the browser
            // picks up the starting position before animating
            this.getBoundingClientRect();

            this.handdrawnLength = length;
          }
        });


    },
    initTransitions: function() {
      var me = this;
      function _initTransitionOfItem(item, offset) {
        var delay = offset,
          length = item.handdrawnLength;


        if (length > 0) {
          var speed = me.settings.speed, // @todo allow overrides form SVG nodes
            transitionSpeed = length / speed,
            transitionTiming = me.settings.transitionTiming; // @todo allow overrides form SVG nodes

          item.style.transition =
            'stroke-dashoffset ' + transitionSpeed + 's ' + transitionTiming + ' ' + delay + 's';

          delay += transitionSpeed;
        }

        var children = $(item)
          .children('g,path,line,polyline');

        // reverse order of items means last child gets drawn first
        if (me.settings.reverseOrder) {
          children = $(children.get().reverse());
        }

        children
          .each(function(){
            delay = _initTransitionOfItem(this, delay);
          });
        return delay;
      }

      _initTransitionOfItem(this.element, 0);
    },

    start: function() {
      this._lines.css('strokeDashoffset', 0);
    }
  });

  // A really lightweight plugin wrapper around the constructor,
  // preventing against multiple instantiations
  $.fn[ pluginName ] = function ( options ) {
    return this.each(function() {
      var plugin = $.data( this, "plugin_" + pluginName );
      if ( !plugin ) {
        $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
      }
      else {
        if (typeof options === 'string') {
          plugin[options]();
        }
      }
    });
  };

})( jQuery, window, document );