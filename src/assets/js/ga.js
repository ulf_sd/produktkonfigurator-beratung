function ga_send(path) {
  ga('send', 'pageview', ga_prefix + path);
}

function ga_escape(str) {
  return encodeURIComponent(str);
}


// track external links

$(document).on('click', 'a.external', function(e) {

//  e.preventDefault();
  var url = $(this).attr('href');
  ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
    function () {
//      document.location = url;
    }
  });
});