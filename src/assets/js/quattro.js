function updateCookieNote () {
	/* 	Cookie disclaimer - for fast hiding without blinking */
	if (localStorage.getItem('cookies-acepted')) {
		document.getElementById("cookie-note").style.display = "none";
	} else {
		document.getElementById("cookie-note").style.display = "block";
	}
}

updateCookieNote ();

$(document).ready(function(){

	updateCookieNote ()

	// Cookie disclaimer
	$('#cookie-close').click(function(e){
		e.preventDefault();
		$('#cookie-note').slideUp();
		localStorage.setItem('cookies-acepted', '1');
	});

	// datenschutz
	$('.privacy-link').click(function(e){
		e.preventDefault();
		$([document.documentElement, document.body]).animate({
			scrollTop: $("#toggle-datenschutz").offset().top
		}, 500);
		if ($("#show-datenschutz-text").is(":visible") ) {
			$( "#show-datenschutz-text" ).trigger( "click" );
		}
	});





// load css
// 	var cb = function() {
// 		var l = document.createElement('link'); l.rel = 'stylesheet';
// 		l.href = '/assets/css/main.css';
// 		var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h);
// 	};
// 	var raf = requestAnimationFrame || mozRequestAnimationFrame ||
// 		webkitRequestAnimationFrame || msRequestAnimationFrame;
// 	if (raf) raf(cb);
// 	else window.addEventListener('load', cb);


	$('svg.handdrawn').css('opacity','1');

	/*============================================
	Page Preloader
	==============================================*/

	// $(window).load(function(){
	// 	$('#page-loader').fadeOut(500);
	// });

	/*============================================
	Parallax Backgrounds
	==============================================*/
	$('.parallax-bg').each(function(){
		var bg = $(this).data('parallax-background');
		$(this).css({'background-image':'url('+bg+')'});

	});

	if((!Modernizr.touch) && ( $(window).width() > 1024) ){
		$(window).stellar({
			horizontalScrolling: false,
			responsive:true
		});
	}
	/*============================================
	Header
	==============================================*/
	//
	//var speed = $('.header-slider').data('speed') ? parseInt($('.header-slider').data('speed'),10) : 3000;
	//
	//$('.header-slider').flexslider({
	//	animation: "fade",
	//	directionNav: false,
	//	controlNav: false,
	//	slideshowSpeed: speed,
	//	animationSpeed: 400,
	//	pauseOnHover:false,
	//	pauseOnAction:true,
	//	smoothHeight: false,
	//	slideshow:false
	//});
	//
	//$(window).load(function(){
	//	if($('.header-slider').length){
	//		$('.header-slider').flexslider('play');
	//	}
	//});

	/*============================================
	Inner Page Header Animation
	==============================================*/
	$(window).scroll( function() {
		var st = $(this).scrollTop();
		$('.no-touch #inner-page-header .header-content').css({ 'opacity' : (1 - st/350) , 'transform':'translateY('+st/3+'px)'});
	});

	/*============================================
	ScrollTo Links
	==============================================*/
	$('a.scrollto').click(function(e){

    var href = $(this).attr('href') || $(this).attr('xlink:href'),
      hash = href ? href.split('#')[1] : null;
    if (!hash) {
      return;
    }
    hash = '#' + hash;
		$('html,body').scrollTo(hash, hash, {gap: {y: this.hash=='#contact'? -65 : 0} ,animation:  {easing: 'easeInOutCubic', duration: 1000}});
		e.preventDefault();

		if ($('.navbar-collapse').hasClass('in')){
			$('.navbar-collapse').removeClass('in').addClass('collapse');
		}
	});


	/*============================================
	Counters
	==============================================*/
	$('.counters').waypoint(function(){
		$('.counter').each(count);
	},{offset:'100%'});

	function count(options) {
		var $this = $(this);
		options = $.extend({}, options || {}, $this.data('countToOptions') || {});
		$this.countTo(options);
	}

	/*============================================
	Project thumbs - Masonry
	==============================================*/
	$(window).load(function(){

		if($('#projects-container').length){
			$('#projects-container').css({visibility:'visible'});

			$('#projects-container').masonry({
				itemSelector: '.project-item:not(.filtered)',
				isFitWidth: false,
				isResizable: true,
				isAnimated: !Modernizr.csstransitions,
				gutterWidth: 0
			});

			scrollSpyRefresh();
			waypointsRefresh();
			stellarRefresh();
		}
	});

	/*============================================
	Filter Projects
	==============================================*/
	$('#filter-works a').click(function(e){
		e.preventDefault();

		if($('#project-preview').hasClass('open')){
			closeProject();
		}

		$('#filter-works li').removeClass('active');
		$(this).parent('li').addClass('active');

		var category = $(this).attr('data-filter');

		$('.project-item').each(function(){
			if($(this).is(category)){
				$(this).removeClass('filtered');
			}
			else{
				$(this).addClass('filtered');
			}

			$('#projects-container').masonry('reload');
		});

		scrollSpyRefresh();
		waypointsRefresh();
		stellarRefresh();
	});

	/*============================================
	Project Preview
	==============================================*/
	$('.project-item').click(function(e){
		e.preventDefault();

		var elem = $(this);

		if($('#project-preview').hasClass('open')){
			$('#project-preview').animate({'opacity':0},300);

			setTimeout(function(){
				$('#project-slider').flexslider('destroy');
				buildProject(elem);
			},300);
		}else{
			buildProject(elem);
		}


	});

	function buildProject(elem){

		var	title = elem.find('.project-title').text(),
			descr = elem.find('.project-description').html(),
			slidesHtml = '<ul class="slides">',
			elemDataCont = elem.find('.project-description');

		var hasVideo = false;
		if(elem.find('.project-description').data('video')){
			slidesHtml = slidesHtml + '<li>'+elem.find('.project-description').data('video')+'</li>';
			hasVideo = true;
		}

		if(elem.find('.project-description').data('images')){
			var	slides = elem.find('.project-description').data('images').split(',');

			for (var i = 0; i < slides.length; ++i) {
				slidesHtml = slidesHtml + '<li><img src='+slides[i]+' alt=""></li>';
			}
		}

		slidesHtml = slidesHtml + '</ul>';

		$('#project-title').text(title);
		$('#project-content').html(descr);
		$('#project-slider').html(slidesHtml);

		openProject(hasVideo);

    ga_send('/project/' + ga_escape(title));

  }

	function openProject(hasVideo){

		$('#project-preview').addClass('open');

		setTimeout(function(){
			$('#project-preview').slideDown();

			$('html,body').scrollTo(0,'#filter-works',
				{
					gap:{y:-100},
					animation:{
						duration:400
					}
			});

			$('#project-slider').fitVids().flexslider({
				prevText: '<i class="fa fa-angle-left"></i>',
				nextText: '<i class="fa fa-angle-right"></i>',
				animation: 'slide',
				slideshowSpeed: 3000,
				useCSS: true,
				controlNav: true,
				pauseOnAction: false,
				pauseOnHover: hasVideo ? false : true,
				smoothHeight: false,
				start: function(){
					if(hasVideo){$('#project-slider').find('li.clone').height(1).empty();$('#project-slider').flexslider("pause");};
					$(window).trigger('resize');
					$('#project-preview').animate({'opacity':1},300);
				}
			});

		},300);

	}

	function closeProject(){

		$('#project-preview').removeClass('open');
		$('#project-preview').animate({'opacity':0},300);

		setTimeout(function(){
			$('#project-preview').slideUp();

			$('#project-slider').flexslider('destroy');
			$('#project-slider').empty();

			scrollSpyRefresh();
			waypointsRefresh();
			stellarRefresh();

		},300);

	}

	$('.close-project').click(function(){
		closeProject();
	})


  /*============================================
   Jobs Preview
   ==============================================*/
  $('.job-item').click(function(e){
    e.preventDefault();

    var elem = $(this);

    if($('#job-preview').hasClass('open')){
      $('#job-preview').animate({'opacity':0},300);

      setTimeout(function(){
        buildJob(elem);
      },300);
    }else{
      buildJob(elem);
    }


  });

  function buildJob(elem){

    var	title = elem.find('.job-title').text(),
      descr = elem.find('.job-description').html();

//    $('#job-title').text(title);
    $('#job-content').html(descr);

    openJob();

    ga_send('/job/' + ga_escape(title));
  }

  function openJob(){

    $('#job-preview').addClass('open');

    setTimeout(function(){
      $('#job-preview').slideDown();

      $('html,body').scrollTo(0,'#jobs',
        {
          animation:{
            duration:400
          }
        });

      $('#job-preview').animate({'opacity':1},300);
    },300);

  }

  function closeJob(){

    $('#job-preview').removeClass('open');
    $('#job-preview').animate({'opacity':0},300);

    setTimeout(function(){
      $('#job-preview').slideUp();

      scrollSpyRefresh();
      waypointsRefresh();
      stellarRefresh();

    },300);

  }

  $('.close-job').click(function(){
    closeJob();
  })




  /*============================================
  Testimonials Slider
  ==============================================*/

    if ($('#flexslider-dev').length > 0) {
        $('#flexslider-dev').flexslider({
            prevText: '<i class="fa fa-angle-left"></i>',
            nextText: '<i class="fa fa-angle-right"></i>',
            animation: 'fade',
            slideshowSpeed: 20000,
            animationSpeed: 400,
            useCSS: true,
            directionNav: true,
            pauseOnAction: false,
            pauseOnHover: false,
            smoothHeight: false
        });
        $('#flexslider-dev').flexslider('pause');


        $('.flexslider-play').click(function(){
            var flexId = $(this).data('flexslider'),
                flex = $('#' + flexId),
                hideId = $(this).data('hide'),
                hide = $('#' + hideId);

            hide.hide();
            flex.show().flexslider('play');
        });

        $(window).load(function(){
            $('.flexslider-hide').hide();
        });

    }

  $('#testimonials-slider').flexslider({
    prevText: '<i class="fa fa-angle-left"></i>',
    nextText: '<i class="fa fa-angle-right"></i>',
    animation: 'fade',
    slideshowSpeed: 5000,
    animationSpeed: 400,
    useCSS: true,
    directionNav: false,
    pauseOnAction: false,
    pauseOnHover: true,
    smoothHeight: false
  });


	/*============================================
	Google Map
	==============================================*/

	$(window).load(function(){



		if($('#gmap').length){


			var map;
			var mapstyles = [ { "stylers": [ { "saturation": -100 } ] } ];

			var infoWindow = new google.maps.InfoWindow;

			var pointLatLng = new google.maps.LatLng(mapPoint.lat, mapPoint.lng);

			var isDraggable = $('html').is('.touch') ? false : true; // If document (your website) is wider than

			var mapOptions = {
				zoom: mapPoint.zoom,
				center: pointLatLng,
				zoomControl : true,
				zoomControlOptions: {
					style: google.maps.ZoomControlStyle.LARGE,
					position: google.maps.ControlPosition.LEFT_CENTER
				},
				panControl : false,
				streetViewControl : false,
				mapTypeControl: false,
				overviewMapControl: false,
				scrollwheel: false,
				draggable:isDraggable,
				styles: mapstyles
			}

			map = new google.maps.Map(document.getElementById("gmap"), mapOptions);

			var marker = new google.maps.Marker({
				position: pointLatLng,
				map: map,
				title:mapPoint.linkText,
				icon: mapPoint.icon
			});

			var mapLink = 'https://www.google.com/maps/preview?ll='+mapPoint.lat+','+mapPoint.lng+'&z=14&q='+mapPoint.mapAddress;

			var html = '<div class="infowin">'
					+ mapPoint.infoText
					+ '<a href="'+mapLink+'" target="_blank">'+mapPoint.linkText+'</a>'
					+ '</div>';

			google.maps.event.addListener(marker, 'mouseover', function() {
				infoWindow.setContent(html);
				infoWindow.open(map, marker);
			});

			google.maps.event.addListener(marker, 'click', function() {
				window.open(mapLink,'_blank');
			});

			/*Toggle Map*/

			$('#toggle-map').click(function(){

				if($('#contact').is('.show-map')){

					$('#contact').css({'height':'auto'});
					$('#contact .container,#gmap-overlay').fadeIn();

          ga('send','event', 'Map', 'hide');

				}else{

					$('#contact').height($('#contact').height());
					$('#contact .container,#gmap-overlay').fadeOut();

          ga('send','event', 'Map', 'show');
				}

				$('#contact').toggleClass('show-map');
				$('.show-map-text,.hide-map-text').toggle();
			})

			$("<div id='gmap-overlay'></div>").prependTo($('#gmap'));
		}


    /*Toggle imprint*/
    if($('#imprint').length){
      $('#toggle-imprint').click(function(){
        $('#show-imprint-text,#hide-imprint-text').toggle();
        if($('#imprint').is('.show')){
          $('#imprint .container').slideUp(300);
        } else {
          $('#imprint .container').slideDown(300);
          $('html, body').animate({
            scrollTop: $("#imprint").offset().top
          }, 300);
        }
        $('#imprint').toggleClass('show');
      });
    }


    /*Toggle datenschutz*/
    if($('#datenschutz').length){

      $('#toggle-datenschutz').click(function(){
        $('#show-datenschutz-text,#hide-datenschutz-text').toggle();
        if($('#datenschutz').is('.show')){
          $('#datenschutz .container').slideUp(300);
        } else {
          $('#datenschutz .container').slideDown(300);
          $('html, body').animate({
            scrollTop: $("#datenschutz").offset().top
          }, 300);
        }
        $('#datenschutz').toggleClass('show');
      });
    }




	});
	/*============================================
	Tooltips
	==============================================*/
	$("[data-toggle='tooltip']").tooltip();

	/*============================================
	Placeholder Detection
	==============================================*/
	if (!Modernizr.input.placeholder) {
		$('html').addClass('no-placeholder');
	}

	/*============================================
	Scrolling Animations
	==============================================*/
	$('.scrollimation').waypoint(function(){
		$(this).addClass('in');
	},{offset:'80%'});

  $('section.text-effect p').waypoint(function(){
    $(this).addClass('in');
  },{offset:'90%'});


	/*============================================
	Video functions
	==============================================*/
	$('.video-container').fitVids();

	/*============================================
	Blog post slider
	==============================================*/

	$('.post-slider').flexslider({
		prevText: '<i class="fa fa-angle-left"></i>',
		nextText: '<i class="fa fa-angle-right"></i>',
		animation: "slide",
		directionNav: true,
		controlNav: false,
		slideshowSpeed: 4000,
		animationSpeed: 700,
		pauseOnHover:true,
		pauseOnAction:false,
		smoothHeight: false
	});

	/*============================================
	Resize Functions
	==============================================*/
	$(window).resize(function(){

		if($('#projects-container').length){
			$('#projects-container').masonry('reload');
		}

		stellarRefresh();
		scrollSpyRefresh();
		waypointsRefresh();
		fitDropdown();

	});

	/*============================================
	Refresh scrollSpy function
	==============================================*/
	function scrollSpyRefresh(){
		setTimeout(function(){
			$('body').scrollspy('refresh');
		},1000);

	}

	/*============================================
	Refresh waypoints function
	==============================================*/
	function waypointsRefresh(){
		setTimeout(function(){
			$.waypoints('refresh');
		},1000);
	}

	/*============================================
	Refresh Parallax Backgrounds
	==============================================*/
	function stellarRefresh(){
		setTimeout(function(){
			$(window).stellar('refresh');
		},1000);
	}

	/*============================================
	Fit 2nd level dropdown
	==============================================*/
	function fitDropdown(){
		if($('.dropdown .dropdown').length){
			var od = $('.dropdown .dropdown').offset().left,
				w = $(window).width(),
				wd1 = $('.dropdown-menu .dropdown-menu').parents('.dropdown-menu').width(),
				wd2 = $('.dropdown-menu .dropdown-menu').width();

			if(wd2 > w-od-wd1){
				$('.dropdown .dropdown').addClass('left-side');
			}else{
				$('.dropdown .dropdown').removeClass('left-side');
			}
		}
	}

	fitDropdown();

	$('.dropdown > a').click(function(e){
		if($('html').is('.touch') || ($(window).width()<768)){
			e.preventDefault();

			var $dm = $(this).next('.dropdown-menu');

			if($dm.is('.dropdown-open')){
				$dm.slideUp();
			}else{
				$dm.slideDown();
			}

			$dm.toggleClass('dropdown-open');
		}
	})


  /**
   * Handdrawn SVGs
   */


  $('svg.handdrawn').handdrawn({
    reverseOrder: true,
    autoStart: false
  });

  $('svg.handdrawn').waypoint(function(){
    $(this).handdrawn('start');
  },{offset:'80%'});


});
