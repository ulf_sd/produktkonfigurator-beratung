var Metalsmith = require('metalsmith'),
  markdown   = require('metalsmith-markdown'),
  ignore = require('metalsmith-ignore'),
  collections = require('metalsmith-collections'),
  serve = require('metalsmith-serve'),
  less = require('metalsmith-less'),
  concat = require('metalsmith-concat'),
  uglify = require('metalsmith-uglify'),
  watch = require('metalsmith-watch'),
  permalinks  = require('metalsmith-permalinks'),
  templates  = require('metalsmith-templates'),
  assets = require('metalsmith-assets'),
  drafts = require('metalsmith-drafts');
  multiLanguage = require('metalsmith-multi-language'),
  localizeCollection = require('metalsmith-localize-collection');
  Handlebars = require('handlebars'),
  fs         = require('fs'),
  cleanCSS   = require('metalsmith-clean-css');
  htmlMinifier = require("metalsmith-html-minifier");

Handlebars.registerPartial('header', fs.readFileSync(__dirname + '/templates/partials/header.hbs').toString());
Handlebars.registerPartial('footer', fs.readFileSync(__dirname + '/templates/partials/footer.hbs').toString());
Handlebars.registerPartial('nav', fs.readFileSync(__dirname + '/templates/partials/nav.hbs').toString());
Handlebars.registerPartial('intro', fs.readFileSync(__dirname + '/templates/partials/intro.hbs').toString());
Handlebars.registerPartial('section', fs.readFileSync(__dirname + '/templates/partials/section.hbs').toString());

Handlebars.registerHelper('localize-collection', function(collections, key, locale) {
  return collections[key + '_' + locale];
});

Handlebars.registerHelper('language-chooser', function() {
  var result = [],
    item,
    currentLocale = this.locale

  for(var key in this.altFiles) {
    item = this.altFiles[key];
    if (key !== currentLocale && item) {
      result.push({
        url: item.path,
        locale: item.locale
      });
    }
  }

  return result;
});


module.exports = Metalsmith(__dirname)
  .use(markdown())
  .use(drafts())
  .use(multiLanguage({ default: 'de', locales: ['de', 'en'] }))
  .use(collections({
    beratung: {
      pattern: 'content/blocks/*',
      sortBy: 'sorting'
    }
  }))
  .use(assets({
    source: 'src/assets/domain_root_files',
    destination: ''
  }))
  .use(less({
    pattern: 'assets/less/main.less',
    render: {
      paths: 'src/assets/less'
    }
  }))
  .use(cleanCSS({
    files: 'assets/**/*.css',
  }))
  .use(concat({
    files: [
      'assets/bower_components/jquery/dist/jquery.js',
      'assets/bower_components/bootstrap/dist/js/bootstrap.js',
      'assets/bower_components/modernizr/modernizr.js',
      'assets/js/jquery.easing.1.3.js',
      'assets/js/jquery.scrollto.js',
      'assets/js/jquery.flexslider.js',
      //'assets/bower_components/FlexSlider/jquery.flexslider.js',
      'assets/bower_components/stellar/jquery.stellar.js',
      "assets/bower_components/masonry/jquery.masonry.js",
      "assets/bower_components/waypoints/waypoints.js",
      "assets/bower_components/waypoints/shortcuts/sticky-elements/waypoints-sticky.js",
      //"assets/bower_components/lazy-line-painter/jquery.lazylinepainter-1.5.1.js",
      "assets/js/jquery.handdrawn.js",
      "assets/js/jquery.countTo.js",
      "assets/js/jquery.fitvids.js",
      "assets/js/contact.js",
      "assets/js/quattro.js",
      "assets/js/ga.js"
    ],
    output: 'assets/js/main.js'
  }))
  .use(ignore(['content/blocks/**/*', 'content/projects/**/*', 'content/jobs/**/*', 'assets/bower_components/**/*', 'assets/less/**/*']))
  .use(uglify({
    removeOriginal: true
  }))
  .use(permalinks({
    pattern: ':unit/:locale'
  }))
  .use(templates('handlebars'))

  .use(htmlMinifier()) // Use the default options

  .destination('./dist')
;