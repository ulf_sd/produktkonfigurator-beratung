'use strict';

const uglify = require('metalsmith-uglify');
const htmlMinifier = require("metalsmith-html-minifier");

require('./common.js')
  .use(uglify({
    removeOriginal: true,
    sameName: true
  }))
  .use(htmlMinifier("*.html", {
    removeAttributeQuotes: false
  }))
  .build(function(err) {
    if (err) {
      throw err;
    }
  });

