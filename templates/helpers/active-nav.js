'use strict';

module.exports = function(navUrl, currentUrl) {
  return navUrl.length > 2 && currentUrl.substring(0, navUrl.length ) === navUrl || navUrl === currentUrl;
};
