'use strict';

module.exports = function() {
  return Array.from(arguments)
    .filter(arg => typeof arg === 'string')
    .reduce((res, item) => res + item, '');
};
