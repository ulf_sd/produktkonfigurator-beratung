'use strict';

module.exports = function(collections, key, locale) {
  return collections ? collections[`${key}_${locale}`] : null;
};
