'use strict';

module.exports = function() {
  return this.altFiles && Object.keys(this.altFiles).length > 1;
};
