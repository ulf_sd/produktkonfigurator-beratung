'use strict';

module.exports = function() {
  let currentLocale = this.locale;

  return Object.keys(this.altFiles)
    .filter(key => key !== currentLocale && this.altFiles[key])
    .map(key => this.altFiles[key])
    .map(item => ({
      url: `/${item.path}`,
      locale: item.locale
    }));
};
