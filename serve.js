'use strict';

const serve = require('metalsmith-serve');

require('./common.js')
  .use(serve({
    //host: "192.168.1.100", needed for testing in virtualbox
    port: 8080
  }))
  .build(function(err) {
    if (err) {
      throw err;
    }
  })
;
